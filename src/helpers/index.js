export function getDateTime(input = "") {
  const currentdate = input ? new Date(input) : new Date();
  const datetime =
    currentdate.getFullYear() +
    "-" +
    ("0" + (currentdate.getMonth() + 1)).slice(-2) +
    "-" +
    ("0" + currentdate.getDate()).slice(-2) +
    "T" +
    ("0" + currentdate.getHours()).slice(-2) +
    ":" +
    ("0" + currentdate.getMinutes()).slice(-2);
  return datetime;
}
