import React, { useEffect, useState } from "react";
import { fetchAppData } from "../api/home";
import {
  Grid,
  Paper,
  Button,
  makeStyles,
  Typography,
  GridList,
  GridListTile,
  LinearProgress
} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4),
    margin: theme.spacing(12)
  },
  text: { height: 160, overflow: "hidden" },
  flex: { display: "flex" },
  genre: {
    margin: theme.spacing(1),
    color: "#33691e",
    fontWeight: "700",
    fontSize: "13px",
    cursor: "pointer",
    "&:hover": {
      textDecoration: "underline"
    }
  },
  imageRoot: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-around",
    overflow: "hidden",
    marginTop: theme.spacing(2),
    backgroundColor: theme.palette.background.paper
  },
  gridList: {
    flexWrap: "nowrap",
    transform: "translateZ(0)"
  },
  relative: { position: "relative", marginTop: theme.spacing(2) },
  expand: {
    position: "absolute",
    bottom: 0,
    color: "white",
    fontWeight: "700",
    backgroundColor: "black",
    width: "100%",
    "&:hover": {
      backgroundColor: "black"
    }
  }
}));

const Appetails = props => {
  const classes = useStyles();
  const [info, setInfo] = useState({});
  const [expand, setExpand] = useState(false);
  const [state, setState] = useState({ loading: false, message: "" });

  useEffect(() => {
    setState({ ...state, loading: true, message: "" });
    const pkg =
      props.location.search && props.location.search.split("=")
        ? props.location.search.split("=")[1]
        : "";
    fetchAppData(pkg).then(res => {
      if (res && res.status < 350) {
        setInfo(res.data);
        setState({ ...state, loading: false, message: "" });
      } else {
        setState({ ...state, loading: false, message: "Something went wrong" });
      }
    });
  }, []);

  return (
    <Paper className={classes.root}>
      {info && Object.keys(info).length ? (
        <>
          <Grid container spacing={4}>
            <Grid item xs={3}>
              <img src={info.icon} alt={info.title} width="180" height="180" />
            </Grid>
            <Grid item xs={9} className={classes.text}>
              <Typography variant="h5">{info.title}</Typography>
              <div className={classes.flex}>
                <Typography className={classes.genre} variant="body2">
                  {info.developer}
                </Typography>
                <Typography className={classes.genre} variant="body2">
                  {info.genre}
                </Typography>
              </div>
            </Grid>
          </Grid>
          <div className={classes.imageRoot}>
            <GridList className={classes.gridList} cols={2.5}>
              {info.screenshots &&
                info.screenshots.map(tile => (
                  <GridListTile
                    style={{ width: "18%", height: "320px" }}
                    key={tile}
                  >
                    <img src={tile} alt={tile} />
                  </GridListTile>
                ))}
            </GridList>
          </div>
          <div className={classes.relative}>
            <Typography
              style={
                !expand
                  ? { maxHeight: 180, overflow: "hidden" }
                  : { maxHeight: "none" }
              }
            >
              <span
                dangerouslySetInnerHTML={{ __html: info.descriptionHTML }}
              ></span>
            </Typography>
            <Button
              className={classes.expand}
              onClick={() => setExpand(!expand)}
            >
              {expand ? "Collapse" : "Read More"}
            </Button>
          </div>
          <Typography variant="h5" className={classes.relative}>
            Reviews
          </Typography>
          <ol>
            {info.comments.map((comment, index) => (
              <li key={index}>
                <Typography>{comment}</Typography>
              </li>
            ))}
          </ol>
          <Typography variant="h5" className={classes.relative}>
            What's New
          </Typography>
          <Typography>{info.recentChanges}</Typography>
          <Typography variant="h5" className={classes.relative}>
            ADDITIONAL INFORMATION
          </Typography>
          <Grid container spacing={4}>
            <Grid item xs={12} sm={4}>
              <Typography>Android Version</Typography>
              <Typography>{info.androidVersion}</Typography>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Typography>Size</Typography>
              <Typography>{info.size}</Typography>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Typography>Installs</Typography>
              <Typography>{info.installs}</Typography>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Typography>Current Version</Typography>
              <Typography>{info.version}</Typography>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Typography>Requires Android</Typography>
              <Typography>{info.androidVersionText}</Typography>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Typography>Privacy Policy</Typography>
              <Typography>{info.privacyPolicy}</Typography>
            </Grid>
          </Grid>
        </>
      ) : !state.loading ? (
        <Typography variant="h3">Invalid Package Name</Typography>
      ) : (
        <LinearProgress />
      )}
    </Paper>
  );
};

export default Appetails;
