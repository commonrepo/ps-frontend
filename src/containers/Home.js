import React, { useEffect, useState } from "react";
import { Grid, Paper, makeStyles, LinearProgress } from "@material-ui/core";
import { fetchAllData } from "../api/home";
import Notify from "../components/Notify";
import CardComp from "../components/CardComp";
import Pagination from "@material-ui/lab/Pagination";

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(4),
    margin: theme.spacing(4),
    backgroundColor: "#EEEEEE"
  },
  card: { minHeight: 200 },
  image: {
    height: 152,
    margin: theme.spacing(2),
    "&:hover": {
      opacity: "0.5"
    }
  },
  pagination: { backgroundColor: "white", padding: theme.spacing(2) },
  content: {
    maxHeight: "1.4em",
    overflow: "hidden",
    "&:hover": {
      textDecoration: "underline",
      cursor: "pointer"
    }
  },
  link: { textDecoration: "none" }
}));

const Home = () => {
  const [count, setCount] = useState(0);
  const [page, setPage] = useState(1);
  const [results, setResults] = useState([]);
  const [state, setState] = useState({ loading: false, message: "" });

  const handleAction = page => {
    setState({ ...state, loading: true, message: "" });
    fetchAllData(page).then(res => {
      if (res && res.status < 350 && res.data) {
        setCount(res.data.count);
        setResults(res.data.data);
        setState({ ...state, loading: false, message: "" });
      } else {
        setState({ ...state, loading: false, message: "Something went wrong" });
      }
    });
  };

  useEffect(() => {
    handleAction(page);
  }, [page]);

  const classes = useStyles();

  return (
    <>
      {state.loading && <LinearProgress />}
      {state.message && <Notify message={state.message} />}
      <Paper className={classes.root}>
        <Grid container spacing={2} alignItems="stretch">
          {results &&
            results.map(result => (
              <CardComp key={result.appId} result={result} classes={classes} />
            ))}
        </Grid>
        <Pagination
          className={classes.pagination}
          page={page}
          showFirstButton
          showLastButton
          count={count || 0}
          color="primary"
          onChange={(e, pageNum) => setPage(pageNum)}
        />
      </Paper>
    </>
  );
};

export default Home;
