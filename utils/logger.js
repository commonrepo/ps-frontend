var winston = require("winston");
require("winston-daily-rotate-file");
var transport = new winston.transports.DailyRotateFile({
  filename: "./logs/node-web.log",
  datePattern: "yyyy-MM-dd.",
  prepend: true,
  level: "info"
});

var logger = winston.createLogger({
  transports: transport,
  exitOnError: false
});
module.exports = logger;
