import React from "react";
import {
  Card,
  Grid,
  CardMedia,
  Typography,
  CardContent
} from "@material-ui/core";
import { Link } from "react-router-dom";
import { Rating } from "@material-ui/lab";

const CardComp = props => {
  const { classes, result } = props;
  return (
    <Grid item xs={6} sm={4} md={2}>
      <Link
        to={"/appdetails?pkg=" + result.appId}
        target="_blank"
        className={classes.link}
      >
        <Card className={classes.card}>
          <CardMedia
            className={classes.image}
            image={result.icon}
            title={result.title}
          />
          <CardContent>
            <Typography
              variant="button"
              component="p"
              className={classes.content}
              title={result.title}
            >
              {result.title}
            </Typography>
            <Typography
              variant="caption"
              component="p"
              className={classes.content}
            >
              {result.developer}
            </Typography>
            <Rating
              value={parseFloat(result.scoreText)}
              size="small"
              readOnly
            />
          </CardContent>
        </Card>
      </Link>
    </Grid>
  );
};

export default CardComp;
